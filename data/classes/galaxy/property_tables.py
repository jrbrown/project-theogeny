# 22/06/2020
# Jason Brown
# Contains data for various property values


from data.util import *


# Class used to house property tables, not sure if this best method
class PropertyTables:

    @staticmethod
    def population_description(_object):
        return comma_number((10**_object.population)*_object.population_modifier)

    @classmethod
    def get_property_description(cls, key, value, _object):
        # Determine which field of property table houses description
        descriptive_field = cls.descriptor_field_dict[key]

        # Determine if a special function is required and if so give it the full object to work with
        if descriptive_field == "use_custom_func":
            return getattr(cls, f"{key}_description")(_object)

        # If it's not meant to be displayed we should return None
        elif descriptive_field is None:
            return None

        # Else get the property from the correct field of the value of the correct table
        else:
            # Get the property table for the current property
            property_table = getattr(cls, key)
            return property_table[value][descriptive_field]


    @classmethod
    def get_government_name(cls, name, gov_type, npr=None):
        # Gets the full name of a polity given its gov type, e.g. Eigen --> The Eigen Corporation
        # Might be worth using some of this stuff in a later version:
        # https://forum.paradoxplaza.com/forum/threads/assemble-your-empire-name-from-these-combinations.923709/

        # Add more at some point
        gov_name_types = {
            "self": ["NAME"],
            "company": ["The NAME Corporation/Company/Foundation", "NAME Inc./Co./Corp."],
            "democratic": ["The NAME Republic/Federation", "Republic/Federation of NAME"],
            "oligarchy": ["The Council/Directorate of NAME", "The NAME Council/Directorate"],
            "balkan": ["The NAME Clans/States"],
            "autocracy": ["The NAME Rulership/Kingdom/Principality/Empire", "Imperium/Kingdom/Principality/Empire of NAME"],
            "religous": ["The Enlightened/Holy Dominion of NAME"]
        }

        gov_name_base_selection = gov_name_types[cls.government[gov_type]["names"]]
        choices = list(zip(gov_name_base_selection, np.ones(len(gov_name_base_selection))))
        gov_name_base = choose_from_list(choices, npr)  # This return one of the list entries in above dict

        gov_name_base_words = gov_name_base.split(" ")
        gov_name_words = []

        for word in gov_name_base_words:
            if word == "NAME":
                gov_name_words.append(name)
            elif "/" in word:
                word_selection = word.split("/")
                choices = list(zip(word_selection, np.ones(len(word_selection))))
                gov_name_words.append(choose_from_list(choices, npr))
            else:
                gov_name_words.append(word)

        gov_name = ' '.join(gov_name_words)
        return gov_name

        pass


    # This dictionary tells the get_property_description function where to look
    # If value is "use_custom_func" it will look for a function with the name "{key}_description"
    # If the value is None it's not meant to be displayed
    descriptor_field_dict = {
        "size": "circumference",
        "atmosphere": "atmosphere",
        "hydrographics": "description",
        "population": "use_custom_func",
        "population_modifier": None,
        "starport": "descriptor",
        "government": "government",
        "law_level": "descriptor",
        "tech_level": "descriptor"
    }

    # Gravity is relative to earth
    size = [
        {"circumference": "800 km", "gravity": 0},
        {"circumference": "1600 km", "gravity": 0.05},
        {"circumference": "3200 km", "gravity": 0.15},
        {"circumference": "4800 km", "gravity": 0.25},
        {"circumference": "6400 km", "gravity": 0.35},
        {"circumference": "8000 km", "gravity": 0.45},
        {"circumference": "9600 km", "gravity": 0.7},
        {"circumference": "11200 km", "gravity": 0.9},
        {"circumference": "12800 km", "gravity": 1.0},
        {"circumference": "14400 km", "gravity": 1.25},
        {"circumference": "16000 km", "gravity": 1.4}
    ]

    atmosphere = [
        {"atmosphere": "None", "pressure": "0.00", "req_survival_gear": ["Vacc Suit"]},
        {"atmosphere": "Trace", "pressure": "0.0001 to 0.09", "req_survival_gear": ["Vacc Suit"]},
        {"atmosphere": "Very Thin, Tainted", "pressure": "0.1 to 0.42", "req_survival_gear": ["Respirator", "Filter"]},
        {"atmosphere": "Very Thin", "pressure": "0.1 to 0.42", "req_survival_gear": ["Respirator"]},
        {"atmosphere": "Thin, Tainted", "pressure": "0.43 to 0.7", "req_survival_gear": ["Filter"]},
        {"atmosphere": "Thin", "pressure": "0.43 to 0.7", "req_survival_gear": []},
        {"atmosphere": "Standard", "pressure": "0.71 to 1.49", "req_survival_gear": []},
        {"atmosphere": "Standard, Tainted", "pressure": "0.71 to 1.49", "req_survival_gear": ["Filter"]},
        {"atmosphere": "Dense", "pressure": "1.50 to 2.49", "req_survival_gear": []},
        {"atmosphere": "Dense, Tainted", "pressure": "1.50 to 2.49", "req_survival_gear": ["Filter"]},
        {"atmosphere": "Exotic", "pressure": "Varies", "req_survival_gear": ["Air Supply"]},
        {"atmosphere": "Corrosive", "pressure": "Varies", "req_survival_gear": ["Vacc Suit"]},
        {"atmosphere": "Insidious", "pressure": "Varies", "req_survival_gear": ["Vacc Suit"]},
        {"atmosphere": "Dense, High", "pressure": "2.5+", "req_survival_gear": []},
        {"atmosphere": "Thin, Low", "pressure": "0.5 or less", "req_survival_gear": []},
        {"atmosphere": "Unusual", "pressure": "Varies", "req_survival_gear": ["Varies"]}
    ]

    hydrographics = [
        {"hydrographic_percentage": "0-5%", "description": "Desert world"},
        {"hydrographic_percentage": "6-15%", "description": "Dry world"},
        {"hydrographic_percentage": "16-25%", "description": "A few small seas"},
        {"hydrographic_percentage": "26-35%", "description": "Small seas and oceans"},
        {"hydrographic_percentage": "36-45%", "description": "Wet world"},
        {"hydrographic_percentage": "46-55%", "description": "Large oceans"},
        {"hydrographic_percentage": "56-65%", "description": "Large, interconnected oceans"},
        {"hydrographic_percentage": "66-75%", "description": "Earth-like world"},
        {"hydrographic_percentage": "76-85%", "description": "Water world"},
        {"hydrographic_percentage": "85-95%", "description": "Only a few small islands and archipelagos"},
        {"hydrographic_percentage": "96-100%", "description": "Almost entirely water"}
    ]

    population = [
        {"population": "None", "range": "0", "comparison": "Empty"},
        {"population": "Few", "range": "10+", "comparison": "A tiny farmstead or single family"},
        {"population": "Hundreds", "range": "100+", "comparison": "A village"},
        {"population": "Thousands", "range": "1000+", "comparison": "A large village"},
        {"population": "Tens of thousands", "range": "10000+", "comparison": "A town"},
        {"population": "Hundreds of thousands", "range": "100000+", "comparison": "Average city"},
        {"population": "Millions", "range": "1000000+", "comparison": "Large city"},
        {"population": "Tens of millions", "range": "10000000+", "comparison": "Small country"},
        {"population": "Hundreds of millions", "range": "100000000+", "comparison": "Large country or union"},
        {"population": "Billions", "range": "1000000000+", "comparison": "Present day Earth"},
        {"population": "Tens of billions", "range": "10000000000+", "comparison": "A much more densely populated Earth"}
    ]

    starport = [
        {"class": "X", "descriptor": "None", "best_fuel": "None", "annual_maintenance": "No", "shipyard_capacity": "None", "possible_bases": []},
        {"class": "E", "descriptor": "Frontier (E)", "best_fuel": "None", "annual_maintenance": "No", "shipyard_capacity": "None", "possible_bases": []},
        {"class": "D", "descriptor": "Poor (D)", "best_fuel": "Unrefined", "annual_maintenance": "No", "shipyard_capacity": "None", "possible_bases": ["Scout"]},
        {"class": "C", "descriptor": "Routine (C)", "best_fuel": "Unrefined", "annual_maintenance": "No", "shipyard_capacity": "Can perform reasonable repairs", "possible_bases": ["Scout"]},
        {"class": "B", "descriptor": "Good (B)", "best_fuel": "Refined", "annual_maintenance": "Yes", "shipyard_capacity": "Can construct non-starships", "possible_bases": ["Naval", "Scout"]},
        {"class": "A", "descriptor": "Excellent (A)", "best_fuel": "Refined", "annual_maintenance": "Yes", "shipyard_capacity": "Can construct starships and non-starships", "possible_bases": ["Naval", "Scout"]}
    ]

    government = [
        {"government": "None", "names": "self"},
        {"government": "Company / Corporation", "names": "company"},
        {"government": "Participating Democracy", "names": "democratic"},
        {"government": "Self-Perpetuating Oligarchy", "names": "oligarchy"},
        {"government": "Representative Democracy", "names": "democratic"},
        {"government": "Feudal Technocracy", "names": "oligarchy"},
        {"government": "Captive Government", "names": "democratic"},
        {"government": "Balkanization", "names": "balkan"},
        {"government": "Civil Service Bureaucracy", "names": "democratic"},
        {"government": "Impersonal Bureaucracy", "names": "democratic"},
        {"government": "Charismatic Dictator", "names": "autocracy"},
        {"government": "Non-Charismatic Leader", "names": "autocracy"},
        {"government": "Charismatic Oligarchy", "names": "oligarchy"},
        {"government": "Religious Dictator", "names": "religous"},
        {"government": "Religious Autocracy", "names": "religous"},
        {"government": "Totalitarian Oligarchy", "names": "oligarchy"}
    ]

    law_level = [
        {"descriptor": "No law", "restrictions": "None, Amber Zone candidate"},
        {"descriptor": "Low law I", "restrictions": "Poison gas, Explosives, Undetectable weapons, WMD's"},
        {"descriptor": "Low law II", "restrictions": "Portable energy weapons (excl. ship mounted)"},
        {"descriptor": "Low law III", "restrictions": "Heavy weapons"},
        {"descriptor": "Medium law I", "restrictions": "Light assault weapons, Submachine guns"},
        {"descriptor": "Medium law II", "restrictions": "Personal concealable weapons"},
        {"descriptor": "Medium law III", "restrictions": "All firearms except shotguns and stunners"},
        {"descriptor": "High law I", "restrictions": "Shotguns and all other firearms"},
        {"descriptor": "High law II", "restrictions": "All bladed weapons, stunners"},
        {"descriptor": "High law III", "restrictions": "Any weapons outside one's residence, Amber Zone candidate"},
        {"descriptor": "Extreme law", "restrictions": "Any weapon, Amber Zone candidate"}
    ]

    tech_level = [
        {"descriptor": "None", "characteristics": "No technology"},
        {"descriptor": "Primitive I", "characteristics": "Roughly on par with Bronze or Iron age technology"},
        {"descriptor": "Primitive II", "characteristics": "Renaissance technology"},
        {"descriptor": "Primitive III", "characteristics": "Mass production allows for product standardization, bringing the germ of industrial revolution and steam power"},
        {"descriptor": "Industrial I", "characteristics": "Transition to industrial revolution is complete, bringing plastics, radio and other such inventions"},
        {"descriptor": "Industrial II", "characteristics": "Widespread electrification, tele-communications and internal combustion"},
        {"descriptor": "Industrial III", "characteristics": "Development of fission power and more advanced computing"},
        {"descriptor": "Pre-Stellar I", "characteristics": "Can reach orbit reliably and has telecommunications satellites"},
        {"descriptor": "Pre-Stellar II", "characteristics": "Possible to reach other worlds in the same system, although terraforming or full colonization is not within the culture's capacity"},
        {"descriptor": "Pre-Stellar III", "characteristics": "Development of gravity manipulation, which makes space travel vastly safer and faster, first steps into Jum Drive technology"},
        {"descriptor": "Early Stellar I", "characteristics": "With the advent of Jump, nearby systems are opened up"},
        {"descriptor": "Early Stellar II", "characteristics": "The first primitive (non-creative) artificial intelligence becomes possible in the form of \"low autonomous\" interfaces, as computers begin to model synaptic networks"},
        {"descriptor": "Average Stellar I", "characteristics": "Weather control revolutionises terraforming and agriculture"},
        {"descriptor": "Average Stellar II", "characteristics": "The battle dress appears on the battlefield in response to the new weapons. \"High autonomous\" interfaces allow computers to become self-actuating and self-teaching"},
        {"descriptor": "Average Stellar III", "characteristics": "Fusion weapons become man-portable"},
        {"descriptor": "High Stellar", "characteristics": "Black globe generators suggest a new direction for defensive technologies, while the development of synthetic anagathics means that the human lifespan now vastly increased"}
    ]
