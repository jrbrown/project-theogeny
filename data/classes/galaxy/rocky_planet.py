# 07/07/2020
# Jason Brown
# Class for rocky planets


from data.classes.galaxy.star_system_body import StarSystemBody
from data.classes.galaxy.property_tables import PropertyTables
from data.random_name_gen import get_rand_length_word as rng_name


class RockyPlanet(StarSystemBody):

    cepheus_class_properties = {
        "size": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],  # See choose_from_list, these are relative probabilities
        "atmosphere": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],
        "hydrographics": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],
        "population": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],
        "population_modifier": [0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        "starport": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],
        "government": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],
        "law_level": [1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1],
        "tech_level": [0, 1, 1, 1, 1, 1, 1]
    }

    # Defaults so pycharm doesn't complain about unresolved attributes
    size = 0
    atmosphere = 0
    hydrographics = 0
    population = 0
    population_modifier = 0
    starport = 0
    government = 0
    law_level = 0
    tech_level = 0

    other_class_properties = {"type", "moons"}
    type = "rocky_planet"

    def __init__(self, parent_name=None, number=-1, moons=0, npr=None):
        super().__init__(parent_name=parent_name, number=number, moons=moons, npr=npr)
        self.name_suffix = '-' + str(number + 1)
        self.update_name_with_new_parent()
        self.trade_codes = []
        self.update_trade_codes()

        if self.population != 0:
            self.name = rng_name(npr=self.npr).capitalize()
            self.gov_name = PropertyTables.get_government_name(rng_name(npr=self.npr).capitalize(), self.government, npr=self.npr)


    def update_trade_codes(self):
        trade_codes = []
        size = self.size
        atm = self.atmosphere
        hydro = self.hydrographics
        pop = self.population
        gov = self.government
        law = self.law_level
        tec = self.tech_level

        if atm in range(4, 10) and hydro in range(4, 9) and pop in range(5, 8):
            trade_codes.append("agricultural")

        if size == 0 and atm == 0 and hydro == 0:
            trade_codes.append("asteroid")

        if pop == 0 and gov == 0 and law == 0:
            trade_codes.append("barren")

        if atm >= 2 and hydro == 0:
            trade_codes.append("desert")

        if atm >= 10 and hydro >= 1:
            trade_codes.append("fluid_oceans")

        if atm in [5, 6, 8] and hydro in range(4, 10) and pop in range(4, 9):
            trade_codes.append("garden")

        if pop >= 9:
            trade_codes.append("high_population")

        if tec >= 12:
            trade_codes.append("high_technology")

        if atm in [0, 1] and hydro >= 1:
            trade_codes.append("ice_capped")

        if atm in [0, 1, 2, 4, 7, 9] and pop >= 9:
            trade_codes.append("industrial")

        if pop in [1, 2, 3]:
            trade_codes.append("low_population")

        if tec <= 5:
            trade_codes.append("low_technology")

        if atm in range(0, 4) and hydro in range(0, 4) and pop >= 6:
            trade_codes.append("non-agricultural")

        if pop in [4, 5, 6]:
            trade_codes.append("non-industrial")

        if atm in range(2, 6) and hydro in range(0, 4):
            trade_codes.append("poor")

        if atm in [6, 8] and pop in [6, 7, 8]:
            trade_codes.append("rich")

        if hydro == 10:
            trade_codes.append("water_world")

        if atm == 0:
            trade_codes.append("vacuum")

        self.trade_codes = trade_codes


    def apply_modifiers(self, k, v, npr):
        # This emulates the dice modifiers and restrictions as described in the Cepheus Engine SRD
        # -5's convert from 2D6-2 roll to 2D6-7 roll


        def size_mod(value):
            return value


        def atmosphere_mod(value):
            value -= 5
            value += self.size

            if value < 0 or self.size == 0:
                value = 0
            else:
                value = min(value, 15)
            return value


        def hydrographics_mod(value):
            value -= 5
            value += self.size

            # Must be 0 if size is 0 or 1
            if self.size in [0, 1]:
                value = 0
            elif self.atmosphere in [0, 1, 10, 11, 12]:
                value -= 4
            elif self.atmosphere == 14:
                value -= 2

            value = max(value, 0)
            value = min(value, 10)

            return value


        def population_mod(value):
            if self.size <= 2:
                value -= 1

            if self.atmosphere >= 10:
                value -= 2
            elif self.atmosphere == 6:
                value += 3
            elif self.atmosphere in [5, 8]:
                value += 1
            elif self.hydrographics == 0 and self.atmosphere < 3:
                value -= 2

            # Chance planet is uninhabited regardless, parameterize?
            if npr.random() > 0.3:
                value = 0

            value = max(value, 0)
            value = min(value, 10)
            return value


        def population_modifier_mod(value):
            if self.population == 0:
                value = 0
            return value


        def starport_mod(value):
            value -= 5
            value += self.population

            if value <= 2 or self.population == 0:
                value = 0
            elif value in [3, 4]:
                value = 1
            elif value in [5, 6]:
                value = 2
            elif value in [7, 8]:
                value = 3
            elif value in [9, 10]:
                value = 4
            elif value >= 11:
                value = 5
            return value


        def government_mod(value):
            value -= 5
            value += self.population

            if value < 0 or self.population == 0:
                value = 0
            else:
                value = min(value, 15)
            return value


        def law_level_mod(value):
            value -= 5
            value += self.government

            if value < 0 or self.government == 0:
                value = 0
            else:
                value = min(value, 10)
            return value


        def tech_level_mod(value):
            if self.starport == 0:
                value -= 4
            elif self.starport == 3:
                value += 2
            elif self.starport == 4:
                value += 4
            elif self.starport == 5:
                value += 6

            if self.size in [0, 1]:
                value += 2
            elif self.size in [2, 3, 4]:
                value += 1

            if self.atmosphere in [0, 1, 2, 3, 10, 11, 12, 13, 14, 15]:
                value += 1

            if self.hydrographics in [0, 9]:
                value += 1
            elif self.hydrographics == 10:
                value += 2

            if self.population in [1, 2, 3, 4, 5, 9]:
                value += 1
            elif self.population == 10:
                value += 2
            elif self.population == 11:
                value += 3
            elif self.population == 12:
                value += 4

            if self.government in [0, 5]:
                value += 1
            elif self.government == 7:
                value += 2
            elif self.government in [13, 14]:
                value -= 2

            if self.hydrographics in [0, 10] and self.population >= 6:
                value = max(value, 4)
            if self.atmosphere in [4, 7, 9]:
                value = max(value, 5)
            if (self.atmosphere in [0, 1, 2, 3, 10, 11, 12]) or (self.atmosphere in [13, 14] and self.hydrographics == 10):
                value = max(value, 7)

            if self.population == 0:
                value = 0

            value = max(value, 0)
            value = min(value, 15)
            return value


        modifier_functions = {
            "size": size_mod,
            "atmosphere": atmosphere_mod,
            "hydrographics": hydrographics_mod,
            "population": population_mod,
            "population_modifier": population_modifier_mod,
            "starport": starport_mod,
            "government": government_mod,
            "law_level": law_level_mod,
            "tech_level": tech_level_mod
        }

        v = modifier_functions[k](v)

        return v
