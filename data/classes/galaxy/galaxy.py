# 08/05/2020
# Jason Brown
# Galaxy Class


from data.classes.galaxy.star import Star
from data.classes.galaxy.hex import Hex
from data.util import *


class Galaxy:

    def __init__(self, name, seed=None, x_sectors=2, y_sectors=2, sec_x_size=2, sec_y_size=2, sub_sec_x_size=10,
                 sub_sec_y_size=8, star_chance=0.66, chance_factor=1.5, star_chance_max=0.85, sub_sec_star_sd=0,
                 sub_sec_star_sd_bleed=1, sub_sec_star_sd_bleed_range=None, sub_sec_world_sd=0,
                 sub_sec_world_sd_bleed=0, star_sys_params=None):
        """
        :param name: string; Name of galaxy
        :param seed: int; Seed to use for random generator, 128 bit unsigned int
        :param x_sectors: int; Number of sectors in x direction
        :param y_sectors: int; Number of sectors in y direction
        :param sec_x_size: int; subsector width of a sector
        :param sec_y_size: int; subsector height of a sector
        :param sub_sec_x_size: int; x length of a subsector in hexes
        :param sub_sec_y_size: int; y length of a subsector in hexes

        :param star_chance: float; chance for each hex to gen a star at center of universe
        :param chance_factor: float; determines polynomial nature of probability curve,
                0.1 = center spike, 1 = linear decay, 10 = mostly uniform,
        :param star_chance_max: float; Maximum star chance for a given hex

        :param sub_sec_star_sd: float; Standard deviation in subsector star generation
        :param sub_sec_star_sd_bleed: float; Amount of bleed between subsectors star biases
        :param sub_sec_world_sd: float; Standard deviation in subsector world parameters
        :param sub_sec_world_sd_bleed: float; Amount of bleed between subsectors world biases, UNUSED
        :param sub_sec_star_sd_bleed_range: int; How many subsectors up or down bleeding algorithm should use
        :param star_sys_params: dict; Parameters controlling star system generation

        Usage notes:
            For a centralised, fairly uniform galaxy use default values of chance_factor and sub_sec_star_sd
            For a spread out, splotchy galaxy chance_factor >= 5 and sub_sec_star_sd ~= 1 should work nicely
        """

        if sub_sec_star_sd_bleed_range is None:
            sub_sec_star_sd_bleed_range = int(1 + sub_sec_world_sd_bleed)  # Rounds down

        seed_base = '0123456789acbdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

        if seed is None:
            self.seed = np.random.SeedSequence().entropy
            self.seed_rep = convert_base(self.seed, 10, seed_base)  # Up to 39 digits, 128 bit, converts to alphanumeric
        else:
            self.seed_rep = seed
            self.seed = convert_base(self.seed_rep, seed_base, 10)

        self.npr = np.random.Generator(np.random.PCG64(self.seed))  # Use npr as equivalent to np.random

        self.name = name
        self.populated = False

        self.x_sectors = x_sectors
        self.y_sectors = y_sectors
        self.sec_x_size = sec_x_size
        self.sec_y_size = sec_y_size

        self.x_subsectors = x_sectors * sec_x_size
        self.y_subsectors = y_sectors * sec_y_size
        self.sub_sec_x_size = sub_sec_x_size
        self.sub_sec_y_size = sub_sec_y_size

        self.x_size = self.x_subsectors*self.sub_sec_x_size
        self.y_size = self.y_subsectors*self.sub_sec_y_size

        if self.x_size > 999:
            print("WARNING! Large x size of galaxy, some stars will have the same seed")

        if self.y_size > 999:
            print("WARNING! Large y size of galaxy, some stars will have the same seed")

        self.star_chance = star_chance
        self.chance_factor = chance_factor
        self.star_chance_max = star_chance_max
        self.num_stars = 0

        self.sub_sec_star_sd = sub_sec_star_sd
        self.sub_sec_star_sd_bleed = sub_sec_star_sd_bleed
        self.sub_sec_world_sd = sub_sec_world_sd
        self.sub_sec_world_sd_bleed = sub_sec_world_sd_bleed  # UNUSED
        self.sub_sec_star_sd_bleed_range = sub_sec_star_sd_bleed_range

        self.star_map = np.ndarray((self.x_size, self.y_size), dtype=object)
        self.sub_sec_world_biases = np.ndarray((self.x_subsectors, self.y_subsectors), dtype=object)

        if star_sys_params is None:
            star_sys_params = {}

        self.star_sys_params = star_sys_params


    def __str__(self):
        return f"""========================================
{self.name}

Seed: {self.seed_rep}
Sectors: {self.x_sectors}x{self.y_sectors}
Subsectors: {self.x_subsectors}x{self.y_subsectors}
Size: {self.x_size}x{self.y_size}
Stars: {self.num_stars}
========================================"""


    def full_gen(self, verbose=True):

        self.star_gen(verbose=verbose, full_gen=True)

        if verbose:
            print("Populating stars...")

        self.populate_stars()

        if verbose:
            print("Influence system...")

        self.determine_control_zones()
        self.trade_routes()

        if verbose:
            print("Full galaxy generation complete")
            print(f"\n{self}")


    def star_gen(self, verbose=True, full_gen=False):
        if verbose:
            print(f"Generating {self.name}")
            print("Subsector biases...")

        self.gen_world_param_biases()

        if verbose:
            print("Stars...")

        self.gen_stars()

        if verbose and not full_gen:
            print("Star only galaxy generation complete")
            print(f"\n{self}")


    def gen_world_param_biases(self):  # These biases are currently unimplemented on the Star level
        """
        This generates world parameter biases for each subsector (e.g. Pop, size, tech etc...)
        To add:
            Bleed between sectors
            Global offset for whole galaxy (Param given to creation of galaxy object)
        """
        for x in range(self.x_subsectors):
            for y in range(self.y_subsectors):
                self.sub_sec_world_biases[x][y] = {
                    "population": self.npr.normal(0, self.sub_sec_world_sd),
                    "tech_level": self.npr.normal(0, self.sub_sec_world_sd)
                }


    def populate_stars(self):
        self.populated = True
        for x in range(self.x_size):
            for y in range(self.y_size):
                ssx = int(x / self.sub_sec_x_size)
                ssy = int(y / self.sub_sec_y_size)

                if self.star_map[x][y] == 1:
                    self.star_map[x][y] = Star(self.seed, x, y, self.sub_sec_world_biases[ssx][ssy], self.star_sys_params)
                else:
                    self.star_map[x][y] = Hex()  # Empty hex


    def gen_stars(self):
        """
        Generates stars in the galaxy, uses biasing and bleeding to make subsectors and their neighbours more
        or less dense. Density is concentrated in center of galaxy
        """
        # Biases for star chance to make galaxy less uniform if self.sub_sec_star_sd is non zero
        biases = self.npr.normal(0, self.sub_sec_star_sd, (self.y_subsectors, self.x_subsectors))

        # Bias smoothing
        ssx_size = int(self.x_size / self.sub_sec_x_size)
        ssy_size = int(self.y_size / self.sub_sec_y_size)
        smoothed_biases = np.zeros((ssy_size, ssx_size))

        for ssx in range(ssx_size):
            for ssy in range(ssy_size):
                
                bias_bleed = 0  # Value of bias to add from bleeding
                bleed_amount = 0  # Keeps track of total bias potential we've added so we can normalise

                for i in range(-self.sub_sec_star_sd_bleed_range, self.sub_sec_star_sd_bleed_range + 1):
                    for j in range(-self.sub_sec_star_sd_bleed_range, self.sub_sec_star_sd_bleed_range + 1):
                        if abs(i) + abs(j) != 0:  # Stops current subsec being added to bleed
                            try:
                                distance = ((i ** 2) + (j ** 2)) ** 0.5  # Could experiment with this 0.5
                                bias_bleed += (biases[ssy+i][ssx+j] / distance)
                                bleed_amount += 1 / distance
                            except IndexError:  # Deals with bleed into edge subsectors from out of bounds subsectors
                                pass

                # Calc final bias value, normalises bias so higher bleed values don't give higher biases
                bias_bleed = bias_bleed * self.sub_sec_star_sd_bleed
                normalise_factor = 1 + (self.sub_sec_star_sd_bleed * bleed_amount)
                smoothed_biases[ssy][ssx] = float((biases[ssy][ssx] + bias_bleed) / normalise_factor)


        # Required for star presence algorithm
        xhs = self.x_size / 2
        yhs = self.y_size / 2

        for x in range(self.x_size):
            for y in range(self.y_size):

                # Get smoothed bias
                ssx = int(x / self.sub_sec_x_size)
                ssy = int(y / self.sub_sec_y_size)
                bias = smoothed_biases[ssy][ssx]

                # Star presence algorithm
                try:
                    # + 0.5 corrects for discrete offset
                    x_factor = 1 - ((abs(x - xhs + 0.5) / xhs) ** self.chance_factor)
                    y_factor = 1 - ((abs(y - yhs + 0.5) / yhs) ** self.chance_factor)
                    star_chance = max((x_factor * y_factor + bias), 0)  # Clears negative star chance from biasing
                    star_chance = float(self.star_chance * star_chance)
                except ZeroDivisionError:  # Can only occur when chance_factor is negative
                    star_chance = 1

                star_chance = min(star_chance, self.star_chance_max)

                # Negative star_chance inverts stars
                if (np.sign(star_chance) * self.npr.random()) < star_chance:
                    self.num_stars += 1
                    self.star_map[x][y] = 1


    def determine_control_zones(self):
        # Determines who controls what in the galaxy
        pass


    def trade_routes(self):
        # Puts in trade routes
        pass
