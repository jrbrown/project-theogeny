# 09/07/2020
# Jason Brown
# Hex class - Used to hold data about hexes, is a super class of Star


class Hex:

    def __init__(self):
        self.controller = None  # Reference to the entity which controls this hex, helps with border drawing
