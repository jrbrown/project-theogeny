# 11/05/2020
# Jason Brown
# Star Class


from data.util import *
from data.classes.galaxy.rocky_planet import RockyPlanet
from data.classes.galaxy.gas_planet import GasPlanet
from data.classes.galaxy.planetoid_belt import PlanetoidBelt
from data.classes.galaxy.hex import Hex


class Star(Hex):

    def __init__(self, galaxy_seed=None, x_coord=0, y_coord=0, subsector_biases=None, star_sys_params=None, name=None):
        """
        :param galaxy_seed: int; Seed from parent galaxy, 128 bit unsigned int
        :param x_coord: int; x Location of star in star map
        :param y_coord: int; y location of star in star map
        """

        super().__init__()

        if galaxy_seed is None:
            galaxy_seed = np.random.SeedSequence().entropy  # Up to 39 digits, 128 bit

        # To create a unique star seed we assume the x and y coords are always < 1000000
        x_digit_list = list(str(x_coord))  # Converts x_coord into a list of integers
        y_digit_list = list(str(y_coord))  # Converts y_coord into a list of integers
        gal_seed_digit_list = list(str(galaxy_seed))

        # Use LSBs of coords so if > 999999 neighbour stars aren't the same
        seed_digit_list = gal_seed_digit_list[:26] + x_digit_list[-6:] + y_digit_list[-6:]  # Max 38 digit seed
        seed = int(''.join(seed_digit_list))

        self.npr = np.random.Generator(np.random.PCG64(seed))  # Use npr as equivalent to np.random

        seed_base = '0123456789acbdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

        self.seed = seed
        self.seed_rep = convert_base(self.seed, 10, seed_base)
        self.x_coord = x_coord
        self.y_coord = y_coord

        # Uses stars npr to generate a virtually unique 6 digit code in base 36 to act as placeholder name
        if name is None:
            characters = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            name = ''.join([characters[x] for x in self.npr.integers(0, len(characters), 6)])

        self.name = name

        # Just so default arg is not mutable
        if subsector_biases is None:
            subsector_biases = {}

        # Biasing for world parameters
        default_biases = {
            "population": 0,
            "tech_level": 0
        }

        # Any bias not provided by subsector_biases is set to default
        for parameter in default_biases.keys():
            if parameter not in subsector_biases:
                subsector_biases[parameter] = default_biases[parameter]

        if star_sys_params is None:
            star_sys_params = {}

        # This only works as default if a star is created directly, outside of a galaxy object
        default_star_sys_params = {
            "rocky_planets": 3,
            "rocky_planet_moons": 1,
            "gas_giants": 1,
            "gas_giant_moons": 10,
            "planetoid_belts": 2
        }

        # Any parameter not provided by star_sys_params is set to default
        for parameter in default_star_sys_params.keys():
            if parameter not in star_sys_params:
                star_sys_params[parameter] = default_star_sys_params[parameter]

        # Generate rocky planets
        self.rocky_planets = []
        for i in range(self.npr.poisson(star_sys_params["rocky_planets"])):
            moons = self.npr.poisson(star_sys_params["rocky_planet_moons"])
            self.rocky_planets.append(RockyPlanet(parent_name=self.name, number=i, moons=moons, npr=self.npr))

        # Generate gas planets
        self.gas_planets = []
        for i in range(self.npr.poisson(star_sys_params["gas_giants"])):
            moons = self.npr.poisson(star_sys_params["gas_giant_moons"])
            self.gas_planets.append(GasPlanet(parent_name=self.name, number=i, moons=moons, npr=self.npr))

        # Generate planetoid belts
        self.planetoid_belts = []
        for i in range(self.npr.poisson(star_sys_params["planetoid_belts"])):
            self.planetoid_belts.append(PlanetoidBelt(parent_name=self.name, number=i, npr=self.npr))

        # Get bases in the system
        self.bases = []
        self.update_bases()


    def __str__(self):
        return f"""========================================
{self.name}

Type: Star
Coordinates: {self.x_coord}-{self.y_coord}
Rocky Planets: {len(self.rocky_planets)}
Gas Planets: {len(self.gas_planets)}
Planetoid Belts: {len(self.planetoid_belts)}
Bases: {list_to_text([capitalise_and_space(x) for x in self.bases]) if self.bases else 'None'}
========================================"""


    def update_bases(self):
        bases = []

        starports = [planet.starport for planet in self.rocky_planets]

        if not starports:  # No starports means no bases
            return bases

        # Naval base presence
        if max(starports) in [4, 5]:
            # 8+ on 2D6 is 0.42 chance
            roll = self.npr.random()
            if roll <= 0.42:
                bases.append("naval")

        # Scout base presence
        if 0 not in starports and 1 not in starports:  # No class X or E starports
            roll = self.npr.random()

            if max(starports) == 2:  # 7+ on 2D6
                threshold = 0.58
            elif max(starports) == 3:  # 8+
                threshold = 0.42
            elif max(starports) == 4:  # 9+
                threshold = 0.28
            elif max(starports) == 5:  # 10+
                threshold = 0.17
            else:
                raise Exception

            if roll <= threshold:
                bases.append("scout")

        # Pirate base presence
        if "naval" not in bases and 5 not in starports:  # No naval base or A class starport
            roll = self.npr.random()
            if roll <= 0.028:
                bases.append("pirate")

        self.bases = bases


    def rename_star(self):
        rename_object(self)
        name_update = yes_or_no("Update planet names?")
        if name_update:
            planets = self.rocky_planets + self.gas_planets + self.planetoid_belts
            for planet in planets:
                planet.update_name_with_new_parent(self.name)
