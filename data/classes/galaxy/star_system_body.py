# 12/05/2020
# Jason Brown
# Class for general star system bodies


from data.classes.galaxy.property_tables import PropertyTables
from data.util import *


class StarSystemBody:

    # Placeholders, individual classes should overwrite these
    cepheus_class_properties = dict()  # Class properties which are decided via sim of a dice roll
    other_class_properties = set()  # Class properties determined by other methods
    type = "star_sys_body"
    name = "star_sys_body_obj"
    name_suffix = ""
    population = 0  # This allows checking if planet is inhabited, unused otherwise for gas planets and planetoid belts

    """
    Class for anything in the star system except the star itself

    Name conventions:
        Parent star: 6 numbers or uppercase letters
        Rocky Planet: star_name-number
        Gas Planet: star_name-Gnumber
        Moon: star_name-plane_number-lower_case_letter
        Planetoid Belt: star_name-Bnumber
        NB - Numbers start at 1
    """
    def __init__(self, parent_name=None, number=-1, moons=None, npr=None):
        """
        :param npr: numpy random generator object; From parent star to be used for rng
        """
        if npr is None:
            seed = np.random.SeedSequence().entropy
            self.npr = np.random.Generator(np.random.PCG64(seed))
        else:
            self.npr = npr

        if parent_name is None:
            parent_name = "000000"

        self.parent_name = parent_name
        self.number = number
        self.moons = moons

        # Assign properties to body
        for k, v in self.cepheus_class_properties.items():
            value = choose_from_list(v, self.npr, False)  # TO DO: Change this to a dice roll
            value = self.apply_modifiers(k, value, self.npr)
            setattr(self, k, value)

        # Add a super().__init__()  to allow multiple inheritance for child classes


    def apply_modifiers(self, k, value, npr):  # Placeholder function, updated by child classes that use it
        return value


    def update_name_with_new_parent(self, parent_name=None):  # If parent changes name call this to redo name
        if parent_name is None:
            parent_name = self.parent_name
        else:
            self.parent_name = parent_name

        self.name = parent_name + self.name_suffix


    def basic_info_descriptor_dict(self):  # Returns a dict with basic information on body
        # return get_property_descriptions(self)  # OLD - Delete if new works
        descriptor_dict = dict()

        # Assembles dictionary of other_class_properties
        other_class_properties_dict = dict()
        for key in self.other_class_properties:
            other_class_properties_dict[key] = getattr(self, key)

        # Assembles dictionary of cephues_class_properties
        cepheus_class_properties_dict = dict()
        for key in self.cepheus_class_properties.keys():
            cepheus_class_properties_dict[key] = getattr(self, key)

        # Iterate through other_class_properties_dict and add descriptors to descriptor dict
        for key, value in other_class_properties_dict.items():
            if isinstance(value, str):
                value = capitalise_and_space(value)
            descriptor_dict[key] = value

        # Iterate through cepheus_class_properties and add descriptors to descriptor dict
        for key, value in cepheus_class_properties_dict.items():
            # Returns none if not meant to be displayed
            descriptor = PropertyTables.get_property_description(key, value, self)
            if descriptor is not None:
                descriptor_dict[key] = descriptor

        return descriptor_dict


    def detailed_info(self):
        return self.__repr__()  # Update to include more stuff


    def __repr__(self):
        newline = "\n"
        return f"""========================================
{getattr(self, "gov_name", self.name)}
{f'{newline}Planet: {self.name}' if getattr(self, "gov_name", False) else ''}
{dict_to_string(self.basic_info_descriptor_dict(), cap_and_space=True)}
========================================"""

    def __str__(self):
        return self.__repr__()
