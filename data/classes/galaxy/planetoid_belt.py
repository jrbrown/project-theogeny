# 07/07/2020
# Jason Brown
# Class for planetoid belts


from data.classes.galaxy.star_system_body import StarSystemBody


class PlanetoidBelt(StarSystemBody):

    other_class_properties = {"type"}
    type = "planetoid_belt"

    def __init__(self, parent_name=None, number=-1, npr=None):
        super().__init__(parent_name=parent_name, number=number, npr=npr)
        self.name_suffix = '-B' + str(number + 1)
        self.update_name_with_new_parent()
