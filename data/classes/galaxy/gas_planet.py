# 07/07/2020
# Jason Brown
# Class for gas planets


from data.classes.galaxy.star_system_body import StarSystemBody


class GasPlanet(StarSystemBody):

    other_class_properties = {"type", "moons"}
    type = "gas_planet"

    def __init__(self, parent_name=None, number=-1, moons=0, npr=None):
        super().__init__(parent_name=parent_name, number=number, moons=moons, npr=npr)
        self.name_suffix = '-G' + str(number + 1)
        self.update_name_with_new_parent()
