# 05/07/2020
# Jason Brown
# Distribution Plotting


import numpy as np
import matplotlib.pyplot as plt


def smooth(values, box_pts):
    h_box_pts = int(box_pts / 2) + 1
    box = [(h_box_pts**2) - (x**2) for x in range(-h_box_pts, h_box_pts + 1)]
    box = np.array([x/sum(box) for x in box])
    smooth_values = np.convolve(values, box, mode='same')
    return smooth_values


def plot_dist(distribution, ranges, *args, discrete=False, **kwargs):
    points = 100
    size = 100000
    values = distribution(*args, **kwargs, size=size)

    if discrete:
        slices = range(ranges[0], ranges[1]+1)
    else:
        slices = np.linspace(ranges[0], ranges[1], num=points)

    # Iterate through the slices and count values that fell within the current slice
    counts = [len(list(filter(lambda x: True if slices[i] <= x <= slices[i+1] else False, values))) for i in range(len(slices)-1)]

    slice_width = slices[1]-slices[0]

    # Convert counts to probability density
    pd = [x/(slice_width*size) for x in counts]

    # Smooth
    if not discrete:
        counts = smooth(pd, 5)

    # Plot data
    plt.plot(slices[:-1], counts)
    plt.show()


if __name__ == '__main__':
    plot_dist(np.random.normal, [-4, 4], 0, 1)
    # plot_dist(np.random.beta, [0, 1], 0.5, 0.5)
    # plot_dist(np.random.poisson, [0, 10], 1, discrete=True)
    # plot_dist(np.random.beta, [0, 1], 3, 2)
    """Beta function notes:
            a > b for positive skew, a < b for negative skew
            a or b less than one makes it pareto or exponential like respectively
            Sum of a and b:
                Larger sums that are greater than 2 produce a tighter spread normal distribution (if a and b both > 1)
                Sums less than 2 produce bathtub distribution
            If a and b are both equal and greater than 1 it looks like a normal distribution
            Probabilities only given in range [0,1]"""
