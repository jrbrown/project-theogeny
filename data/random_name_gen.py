# 05/07/2020
# Jason Brown
# Random Name Generator Module
# Recommended import: from ... import random_name_gen.get_rand_length_word as rand_name

from data.util import choose_from_list, prompt
from numpy import random

# Using english frequency distributions from http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
# y is split between the two arbitrarily
vowels = [
    ("a", 8.12),
    ("e", 12.02),
    ("i", 7.31),
    ("o", 7.68),
    ("u", 2.88),
    ("y", 0.25)
]

consonants = [
    ("b", 1.49),
    ("c", 2.71),
    ("d", 4.32),
    ("f", 2.30),
    ("g", 2.03),
    ("h", 5.92),
    ("j", 0.10),
    ("k", 0.69),
    ("l", 3.98),
    ("m", 2.61),
    ("n", 6.95),
    ("p", 1.82),
    ("q", 0.11),
    ("r", 6.02),
    ("s", 6.28),
    ("t", 9.10),
    ("v", 1.11),
    ("w", 2.09),
    ("x", 0.17),
    ("y", 1.86),
    ("z", 0.07)
]

starting_probs = [
    ("consonant", 0.6),
    ("vowel", 0.4)
]

following_vowel_probs = [
    ("consonant", 0.85),
    ("vowel", 0.15)
]

following_consonant_probs = [
    ("consonant", 0.2),
    ("vowel", 0.8)
]

type_to_type_dict = {
    "start": starting_probs,
    "vowel": following_vowel_probs,
    "consonant": following_consonant_probs
}

type_to_list_dict = {
    "consonant": consonants,
    "vowel": vowels
}


def pick_next_letter(previous_type="start", npr=None):
    # npr is a numpy generator object
    type_of_letter = choose_from_list(type_to_type_dict[previous_type], npr=npr)
    return choose_from_list(type_to_list_dict[type_of_letter], npr=npr), type_of_letter


def get_word(length):
    word = ""
    letter_type = "start"

    for i in range(length):
        letter, letter_type = pick_next_letter(letter_type)
        word += letter

    return word


def get_rand_length_word(avg_length=6, npr=None, spread=1.5):
    if npr is None:
        npr = random

    # Looking at a better distribution, maybe a beta distribution, maybe discrete xdy roll
    length = max(1, int(npr.normal(avg_length, spread)+0.5))  # Plus 0.5 means int rounds to nearest integer
    return get_word(length)


def prompt_and_print():
    avg_length = prompt("Average length? ", int)
    number = prompt("Number to generate? ", int)
    for word in range(number):
        print(get_rand_length_word(avg_length))


if __name__ == '__main__':
    prompt_and_print()
