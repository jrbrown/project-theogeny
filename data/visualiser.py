# 09/05/2020
# Jason Brown
# Visualiser


import tkinter as tk
from data.classes.galaxy.property_tables import PropertyTables
from data.classes.galaxy.star import Star


def create_map(width=300, height=300, title='Map', bg='white', max_res=None):  # This should probably be a class
    if max_res is None:  # Max res is largest initial window size, user can resize at will
        max_res = [1728, 972]

    # Makes sure our scroll region is entire map
    scroll_width = width
    scroll_height = height

    if width > max_res[0]:
        width = max_res[0]
    if height > max_res[1]:
        height = max_res[1]

    root = tk.Tk()
    root.title(title)
    frame = tk.Frame(root, width=width, height=height)
    frame.pack(expand=True, fill=tk.BOTH)
    canvas = tk.Canvas(frame, bg=bg, width=width, height=height, scrollregion=(0, 0, scroll_width, scroll_height))
    hbar = tk.Scrollbar(frame, orient=tk.HORIZONTAL)
    hbar.pack(side=tk.BOTTOM, fill=tk.X)
    hbar.config(command=canvas.xview)
    vbar = tk.Scrollbar(frame, orient=tk.VERTICAL)
    vbar.pack(side=tk.RIGHT, fill=tk.Y)
    vbar.config(command=canvas.yview)
    canvas.config(xscrollcommand=hbar.set, yscrollcommand=vbar.set)
    canvas.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

    scroll_direction = 'y'  # This and key press/release events allow holding shift or page up to switch scroll axis

    def mouse_wheel(event):
        scroll_amount = int(event.delta/-120)

        # Linux support
        if event.num == 4:
            scroll_amount = 1
        elif event.num == 5:
            scroll_amount = -1

        if scroll_direction == 'y':
            canvas.yview_scroll(scroll_amount, "units")
        elif scroll_direction == 'x':
            canvas.xview_scroll(scroll_amount, "units")

    def key_press(event):
        if event.keycode in [16, 34, 50]:  # Shift, page up or linux shift key
            nonlocal scroll_direction
            scroll_direction = 'x'

    def key_release(event):
        if event.keycode in [16, 34, 50]:  # Shift, page up or linux shift key
            nonlocal scroll_direction
            scroll_direction = 'y'

    root.bind("<MouseWheel>", mouse_wheel)  # Windows scrolling support

    # Linux scrolling support
    root.bind("<Button-4>", mouse_wheel)
    root.bind("<Button-5>", mouse_wheel)

    root.bind("<KeyPress>", key_press)
    root.bind("<KeyRelease>", key_release)

    return canvas


def star_map(galaxy, scale_factor=1, show_hex=True, show_sec=True, show_subsec=True,
             show_unused_coords=True, show_coords=True, dark_mode=False, view_mode="default"):

    colours = ['white', 'light grey', 'grey', 'black']
    if dark_mode:
        colours = colours[::-1]

    hex_w = 50 * scale_factor
    hex_h = hex_w * (1.732 / 2)
    hex_o = hex_w / 4
    hex_hh = hex_h / 2

    canvas_offset = 2

    x_res = (galaxy.x_size * (hex_w - hex_o)) + hex_o + (canvas_offset * 2)
    y_res = (galaxy.y_size * hex_h) + hex_hh + (canvas_offset * 2)

    canvas = create_map(x_res, y_res, f'Star Map for {galaxy.name}', bg=colours[0])

    hexes = []
    stars = []
    sub_sec_coords = []
    sub_sec_lines = []
    sec_lines = []

    # Determines things to draw onto canvas
    for x in range(galaxy.x_size):
        for y in range(galaxy.y_size):
            x_base = (x * (hex_w - hex_o)) + canvas_offset
            y_base = (y * hex_h) + canvas_offset
            if x % 2 == 1:
                y_base += hex_hh
            p1 = x_base + hex_o, y_base
            p2 = x_base + hex_w - hex_o, y_base
            p3 = x_base + hex_w, y_base + hex_hh
            p4 = x_base + hex_w - hex_o, y_base + hex_h
            p5 = x_base + hex_o, y_base + hex_h
            p6 = x_base, y_base + hex_hh
            hexes.append([p1, p2, p3, p4, p5, p6])

            # If unused_coords is True only coords of PoIs added to map
            if show_unused_coords or galaxy.star_map[x][y] is not None:
                sub_sec_coords.append([x_base+(hex_w/2), y_base+hex_h, f"{x}-{y}"])

            # Check if a star is present and if it is are there any inhabited rocky planets
            if isinstance(galaxy.star_map[x][y], Star) or galaxy.star_map[x][y] == 1:
                inhabited_planets = False
                view_mode_parameter = 0
                text = "X"

                # All the view_mode stuff allows the map to be optionally viewed with numbers instead of X's
                # which represent the highest value of that parameter in that system

                try:
                    for planet in galaxy.star_map[x][y].rocky_planets:
                        if view_mode == "starport":
                            view_mode_parameter = max(view_mode_parameter, planet.starport)
                        elif view_mode == "population":
                            view_mode_parameter = max(view_mode_parameter, planet.population)
                        elif view_mode == "technology":
                            view_mode_parameter = max(view_mode_parameter, planet.tech_level)
                        elif view_mode == "worlds":
                            if planet.population != 0:
                                view_mode_parameter += 1

                        if planet.population != 0:
                            inhabited_planets = True

                except AttributeError:  # Should occur if only did star gen and there are no rocky planets
                    inhabited_planets = True  # Should probably put false but no point in everything being greyed-out

                # Display starport letter but use number for everything else
                if view_mode == "starport":
                    text = getattr(PropertyTables, "starport")[view_mode_parameter]["class"]
                elif view_mode != "default":
                    text = view_mode_parameter

                stars.append(([x_base + (hex_w / 2), y_base + hex_hh], inhabited_planets, text))

            # Following 2 if statements put in subsector boundary lines
            if x % galaxy.sub_sec_x_size == 0:
                sub_sec_lines.append([p1, p6, p5])
            if y % galaxy.sub_sec_y_size == 0:
                if x % 2 == 0:
                    sub_sec_lines.append([p6, p1, p2, p3])
                else:
                    sub_sec_lines.append([p1, p2])

            # Following 2 if statements put in sector boundary lines
            if (x / galaxy.sub_sec_x_size) % galaxy.sec_x_size == 0:
                sec_lines.append([p1, p6, p5])
            if (y / galaxy.sub_sec_y_size) % galaxy.sec_y_size == 0:
                if x % 2 == 0:
                    sec_lines.append([p6, p1, p2, p3])
                else:
                    sec_lines.append([p1, p2])

            # Add in sector and subsector lines if we are at far left or bottom
            if x == galaxy.x_size - 1:
                sec_lines.append([p2, p3, p4])
                sub_sec_lines.append([p2, p3, p4])
            if y == galaxy.y_size - 1:
                if x % 2 == 0:
                    sec_lines.append([p5, p4])
                    sub_sec_lines.append([p5, p4])
                else:
                    sec_lines.append([p6, p5, p4, p3])
                    sub_sec_lines.append([p6, p5, p4, p3])

    # Allows for cleaner printing
    if show_hex:
        for points in hexes:
            canvas.create_polygon(points, outline=colours[1], fill=colours[0])
    if show_subsec:
        for line in sub_sec_lines:
            canvas.create_line(line, fill=colours[2])
    if show_sec:
        for line in sec_lines:
            canvas.create_line(line, fill=colours[3], width=2)
    if show_coords:
        for coord in sub_sec_coords:
            canvas.create_text(coord[0:2], anchor="s", text=coord[2], fill=colours[2],
                               font=('arial', int(6 * scale_factor)))

    for star, inhabited, text in stars:
        # If star has an inhabited rocky planet we display it in a higher contrast colour
        if inhabited:
            colour = colours[3]
        else:
            colour = colours[2]
        canvas.create_text(star, text=text, font=('arial', int(14 * scale_factor)), fill=colour)

    tk.mainloop()


def ascii_star_map(galaxy, star='X', not_star=' '):
    """
    :param galaxy: Galaxy object; Galaxy to produce map of
    :param star: string; Text to show a star
    :param not_star: string; Text for not a star

    WARNING! This will produce a differently hex aligned map to other visualisations
    """
    print("Star map:\n")
    output = []

    for y in range(len(galaxy.star_map[0])):
        if y % 2 == 1:
            output.append(' ')
        for x in range(len(galaxy.star_map)):
            if galaxy.star_map[x][y] is not None:
                output.append(star)
            else:
                output.append(not_star)
            output.append(' ')
        output.append('\n')

    output = ''.join(output)
    print(output)
    print(f"{galaxy.num_stars} stars")
