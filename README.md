# Theogeny

Creates galaxies similar to those created by the Cepheus Engine ruleset for the tabletop game Traveller

## Launch

To run Theogeny it is recommended you execute main.py

You can use a python console to execute the various functions but this may cause unexpected errors

## Warning

As the software is an early version expect bugs and runtime errors. Please report any of these found along with the conditions that cause them.

## Technologies
Project is created with:
* Python version: 3.8
* Numpy version: 1.19.2
* Dill version: 0.3.2
