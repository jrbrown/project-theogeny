# 23/06/2020
# Jason Brown
# Theogeny Main


from data.util import *
from data.classes.galaxy.galaxy import Galaxy
from data import visualiser

version = "ALPHA 1.4.3"


# region Maps
def dark_map(galaxy):
    execute_in_parallel(visualiser.star_map, galaxy, scale_factor=0.1, show_hex=False, show_sec=False,
                        show_subsec=False, show_coords=False, dark_mode=True)
    print("Displaying galaxy")
    enter_to_continue()


def hex_map(galaxy, scale=1, show_coords=True):
    view_mode = menu(
        "Viewing mode? Displays highest rating found inside star system",
        [
            ["Default", lambda: "default"],
            ["Starport", lambda: "starport"],
            ["Population", lambda: "population"],
            ["Technology", lambda: "technology"],
            ["Number of populated worlds", lambda: "worlds"]
        ]
    )
    execute_in_parallel(visualiser.star_map, galaxy, scale_factor=scale, show_hex=True, show_sec=True,
                        show_subsec=True, show_coords=show_coords, dark_mode=False, view_mode=view_mode)
    print("Displaying a hex map")
    enter_to_continue()
# endregion


# region Star and Planet related functions
def planet_info(planet):
    print_info(planet)

    menu(
        "Any further actions?",
        [
            ["No", lambda: "break_menu"],
            ["View detailed info (Unimplemented)", print_info, [planet.detailed_info()]],
            ["Rename planet", rename_object, [planet]]
        ],
        repeat=True,
        disable_auto_exit=True
    )


def planet_list_menu(star):
    planets_list = star.rocky_planets + star.gas_planets + star.planetoid_belts
    # menu_options = [[x.name, print_info, [x]] for x in planets_list]

    while True:
        menu_options = []

        for planet in planets_list:
            if planet.population != 0:
                text = f"{planet.name} (I)"
            else:
                text = planet.name
            menu_options.append([text, planet_info, [planet]])

        if menu("Which planet? (I means inhabited)", menu_options, manual_repeating=True) == "break_menu":
            break


def star_info(galaxy):
    x_coord = prompt("X co-ord: ", int)
    y_coord = prompt("Y co-ord: ", int)

    try:
        star = galaxy.fetch_star_data([x_coord, y_coord])

    except IndexError:
        print("Out of bounds")
        return

    print()
    print_info(star)

    if star is not None:
        menu(
            "Any further actions?",
            [
                ["View a planets info", planet_list_menu, [star]],
                ["Rename star", star.rename_star]
            ],
            repeat=True
        )
# endregion


# region Galaxy related functions
def load_galaxy():
    name = prompt("Name of galaxy? ")
    galaxy = load_object(name, "galaxies")
    print(f"{galaxy.name} loaded")
    enter_to_continue()
    galaxy_menu(galaxy)


def save_galaxy(galaxy):
    save_object(galaxy, galaxy.name, "galaxies")
    print(f"{galaxy.name} saved")
    enter_to_continue()


def galaxy_menu(galaxy):
    while menu(
        f"{galaxy.name} is currently loaded. What would you like to do?",
        [

            ["Return to main menu", lambda: "break_menu"],
            ["Populate galaxy (Repopulates if fully generated already)", galaxy.populate_stars],
            ["View galaxy", dark_map, [galaxy]],
            ["View hex map", hex_map, [galaxy]],
            ["View zoomed-out hex map", hex_map, [galaxy], {"scale": 0.4, "show_coords": False}],
            ["View galaxy info", lambda: print_info(galaxy)],
            ["View a stars info", star_info, [galaxy]],
            ["Rename galaxy", rename_object, [galaxy]],
            ["Save current galaxy", save_galaxy, [galaxy]]
        ],
        question_header=1,
        manual_repeating=True,
        disable_auto_exit=True
    ) != "break_menu": pass


def create_galaxy():
    settings_choice = menu(
        "Settings?",
        [
            ["Basic", lambda: "b"],
            ["Sector size control", lambda: "s"],
            ["Star chance control", lambda: "c"],
            ["Full control", lambda: "f"]
        ]
    )

    settings = {
        "name": prompt("Name: "),
        "seed": prompt("Seed (Leave blank for random): "),
        "x_sectors": prompt("X Sectors: ", int),
        "y_sectors": prompt("Y Sectors: ", int)
    }

    if settings["seed"] == "":
        settings["seed"] = None

    if settings_choice == "s" or settings_choice == "f":
        settings.update({
            "sec_x_size": prompt("Subsector size of X sectors: ", int),
            "sec_y_size": prompt("Subsector size of Y sectors: ", int),

        })

    if settings_choice == "b" or settings_choice == "s":
        non_uniform_star_gen = menu(
            "Type of star gen?",
            [
                ["Uniform", lambda: False],
                ["Non-uniform", lambda: True]
            ]
        )

        if non_uniform_star_gen:
            settings.update({
                "sub_sec_star_sd": 1
            })

    if settings_choice == "c" or settings_choice == "f":
        settings.update({
            "star_chance": prompt("Basic star gen chance (default 0.66): ", float),
            "chance_factor": prompt("Star chance factor (default 1.5): ", float),
            "sub_sec_star_sd": prompt("Non-uniformity (default 1.0 for non-uniform): ", float)
        })

    galaxy = Galaxy(**settings)
    print("Galaxy successfully initialised")

    gen_type = menu(
        "Type of generation?",
        [
            ["Just stars", lambda: True],
            ["Full", lambda: False]
        ]
    )

    if gen_type:
        galaxy.star_gen(verbose=False)
    else:
        galaxy.full_gen(verbose=False)

    save_object(galaxy, "autosave", "galaxies")

    print("Galaxy successfully populated and is autosaved")

    galaxy_menu(galaxy)


def default_galaxy():
    galaxy = Galaxy("Default", seed="default")
    galaxy.full_gen(verbose=False)
    galaxy_menu(galaxy)
# endregion


if __name__ == '__main__':
    menu(
        f"Theogeny {version}",
        [
            ["Create a galaxy", create_galaxy],
            ["Create a default galaxy", default_galaxy],
            ["Load a galaxy from galaxy folder", load_galaxy]
        ],
        question_header=2,
        repeat=True
    )
