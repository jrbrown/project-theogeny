# 08/05/2020
# Jason Brown
# Theogeny Tests, nb not unit tests just manual testing of stuff


from data.classes.galaxy.galaxy import Galaxy
from data.classes.galaxy.star import Star
from data.classes.galaxy.rocky_planet import RockyPlanet
from data import visualiser
from data.util import *


def test1():
    test_galaxy = Galaxy("Test Galaxy 1", x_sectors=1, y_sectors=1,
                         sec_x_size=2, sec_y_size=2, chance_factor=3, star_chance=0.66, sub_sec_star_sd=0.2,
                         sub_sec_star_sd_bleed=0.1)
    test_galaxy.full_gen()
    print()
    star = test_galaxy.star_map[0, 0]
    print(star)
    visualiser.star_map(test_galaxy, scale_factor=1, show_hex=True, show_sec=True,
                        show_subsec=True, show_coords=True, dark_mode=False)


def test2():
    test_galaxy = Galaxy("Test Galaxy 2", x_sectors=6, y_sectors=6, sec_x_size=4, sec_y_size=4,
                         chance_factor=1.5, star_chance=0.66, sub_sec_star_sd=0.5, sub_sec_star_sd_bleed=1)
    test_galaxy.full_gen()
    print()
    visualiser.star_map(test_galaxy, scale_factor=0.1, show_hex=False, show_sec=False,
                        show_subsec=False, show_coords=False, dark_mode=True)


def test3():
    test_galaxy = Galaxy("Test Galaxy 3", seed='655fTWFHVh1WVyhliZPjPl', x_sectors=6, y_sectors=6, sec_x_size=4, sec_y_size=4,
                         chance_factor=5, star_chance=0.66, sub_sec_star_sd=1, sub_sec_star_sd_bleed=1)
    test_galaxy.star_gen()
    visualiser.star_map(test_galaxy, scale_factor=0.1, show_hex=False, show_sec=False,
                        show_subsec=False, show_coords=False, dark_mode=True)


def test4():
    test_galaxy = Galaxy("Test Galaxy 4", seed=1, x_sectors=1, y_sectors=1, sub_sec_world_sd=0.1,
                         sec_x_size=1, sec_y_size=1, chance_factor=3, star_chance=0.66)
    test_galaxy.full_gen()
    visualiser.star_map(test_galaxy, scale_factor=1, show_hex=True, show_sec=True,
                        show_subsec=True, show_coords=True, dark_mode=False)


def test5():
    star = Star()
    print(star)

    print("\nRocky planets:")
    for body in star.rocky_planets:
        print(body)
        """if body.moons is not None:
            print("Moons:")
            for moon in body.moons:
                print(moon)"""

    print("\nGas planets:")
    for body in star.gas_planets:
        print(body)
        """if body.moons is not None:
            print("Moons:")
            for moon in body.moons:
                print(moon)"""

    print("\nPlanetoid Belts:")
    for body in star.planetoid_belts:
        print(body)


def test6():
    for i in range(20):
        body = RockyPlanet(parent_name="Test6", number=i)
        # print(body.name, body.property_dict())
        print(body)
        print(body.trade_codes)


def test7():
    def fa():
        print("A")

    def fx(x):
        print(x)

    def f1():
        return "break_menu", 1

    def fex():
        return "break_menu"

    def menu_test_options(x):
        return [
            ["Print A", fa],
            [f"Print {x}", fx, [x]],
            ["Return 1", f1],
            ["Return nothing", fex]
        ]

    print(menu(
        "This is a test menu",
        menu_test_options("Hello World"),
        question_header=2,
        repeat=True
    ))


def test8():
    planet = RockyPlanet(parent_name="Test8")
    print(planet)


def test9():
    for i in range(20):
        star = Star()
        print(star)


if __name__ == '__main__':
    # test1()
    # test2()
    test3()
    # test4()
    # test5()
    # test6()
    # test7()
    # test8()
    # test9()
